import wx
import wx.xrc
import Hash

WIDTH = 600
HEIGHT = 400

class ttMessageDialog(wx.Dialog):
    def __init__(self, message, title, ttl=1):
        wx.Dialog.__init__(self, None, -1, title,size=(300, 100), style=wx.OK)
        self.CenterOnScreen(wx.BOTH)
        self.timeToLive = ttl
        self.SetBackgroundColour((250, 250, 250))
        stdBtnSizer = self.CreateStdDialogButtonSizer(wx.OK)
        stMsg = wx.StaticText(self, -1, message)
        stMsg.SetFont(wx.Font(15, wx.FONTFAMILY_SCRIPT, wx.FONTSTYLE_ITALIC, wx.FONTWEIGHT_BOLD))

        vbox = wx.BoxSizer(wx.VERTICAL)
        vbox.Add(stMsg, 1, wx.ALIGN_CENTER|wx.TOP, 10)
        vbox.Add(stdBtnSizer,1, wx.ALIGN_CENTER|wx.TOP, 10)
        self.SetSizer(vbox)

        self.timer = wx.Timer(self)
        self.timer.Start(1000)#Generate a timer event every second
        self.timeToLive = ttl
        self.Bind(wx.EVT_TIMER, self.onTimer, self.timer)

    def onTimer(self, evt):
        self.timeToLive -= 1
        if self.timeToLive == 0:
            self.timer.Stop()
            self.Destroy()

class MainFrame(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title="Enrypt Files", pos=wx.DefaultPosition, size=wx.Size(WIDTH, HEIGHT), style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER ^ wx.MAXIMIZE_BOX)
        self.filePath = ""
        self.key = ""

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.SetIcon(wx.Icon('security.png'))

        FONT24 = wx.Font(24, wx.FONTFAMILY_ROMAN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD)
        self.FONT18 = wx.Font(18, wx.FONTFAMILY_ROMAN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD)
        FONT15 = wx.Font(15, wx.FONTFAMILY_ROMAN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD)
        FONT12 = wx.Font(12, wx.FONTFAMILY_ROMAN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD)

        self.bSizer = wx.BoxSizer(wx.VERTICAL)

        self.bSizerString = wx.BoxSizer(wx.HORIZONTAL)
        panel = wx.Panel(self)
        self.txtWelcome = wx.StaticText(panel, wx.ID_ANY, "Welcome to Cryption App", (0, 10), (WIDTH-10, 10), wx.ALIGN_CENTER)

        self.txtWelcome.SetFont(FONT24)

        self.bSizerString.Add(self.txtWelcome, 0, wx.ALL, 5)

        self.btnChooseFile = wx.Button(panel, wx.ID_ANY, pos=(50, 150), label="Choose a file", size=(100, 30))
        self.btnChooseFile.SetFont(FONT12)
        self.bSizerString.Add(self.btnChooseFile, 0, wx.ALL, 5)

        self.bSizer.Add(self.bSizerString, 1, wx.EXPAND, 5)
        self.txtFile = wx.StaticText(panel, -1, "Not yet selected", (200, 155))
        self.txtFile.SetMaxSize((300, 50))
        self.txtFile.SetFont(FONT15)
        self.bSizerString.Add(self.txtFile, 0, wx.ALL, 5)


        #MENUBAR
        self.menuBar = wx.MenuBar()

        self.menuHash = wx.Menu()
        self.hashMD2 = wx.MenuItem(self.menuHash, wx.ID_ANY, "MD2")
        self.hashMD4 = wx.MenuItem(self.menuHash, wx.ID_ANY, "MD4")
        self.hashMD5 = wx.MenuItem(self.menuHash, wx.ID_ANY, "MD5")
        self.hashRIPEMD = wx.MenuItem(self.menuHash, wx.ID_ANY, "RIPEMD")
        self.hashSHA1 = wx.MenuItem(self.menuHash, wx.ID_ANY, "SHA1")
        self.hashSHA224 = wx.MenuItem(self.menuHash, wx.ID_ANY, "SHA224")
        self.hashSHA256 = wx.MenuItem(self.menuHash, wx.ID_ANY, "SHA256")
        self.hashSHA384 = wx.MenuItem(self.menuHash, wx.ID_ANY, "SHA384")
        self.hashSHA512 = wx.MenuItem(self.menuHash, wx.ID_ANY, "SHA512")

        self.menuHash.Append(self.hashMD2)
        self.menuHash.Append(self.hashMD4)
        self.menuHash.Append(self.hashMD5)
        self.menuHash.Append(self.hashSHA1)
        self.menuHash.Append(self.hashSHA224)
        self.menuHash.Append(self.hashSHA256)
        self.menuHash.Append(self.hashSHA384)
        self.menuHash.Append(self.hashSHA512)
        self.menuHash.Append(self.hashRIPEMD)

        # Menu Encrypt
        self.menuEncrypt = wx.Menu()

        self.encryptAES = wx.MenuItem(self.menuEncrypt, wx.ID_ANY, "AES")
        self.encryptDES = wx.MenuItem(self.menuEncrypt, wx.ID_ANY, "DES")
        self.encryptDES3 = wx.MenuItem(self.menuEncrypt, wx.ID_ANY, "DES Triple")
        self.encryptARC2 = wx.MenuItem(self.menuEncrypt, wx.ID_ANY, "ARC2")
        self.encryptARC4 = wx.MenuItem(self.menuEncrypt, wx.ID_ANY, "ARC4")
        self.encryptBlowfish = wx.MenuItem(self.menuEncrypt, wx.ID_ANY, "Blowfish")
        self.encryptCAST = wx.MenuItem(self.menuEncrypt, wx.ID_ANY, "CAST")

        self.menuEncrypt.Append(self.encryptAES)
        self.menuEncrypt.Append(self.encryptDES)
        self.menuEncrypt.Append(self.encryptDES3)
        self.menuEncrypt.Append(self.encryptARC2)
        self.menuEncrypt.Append(self.encryptARC4)
        self.menuEncrypt.Append(self.encryptBlowfish)
        self.menuEncrypt.Append(self.encryptCAST)


        # Menu Symmetric Encryption
        self.menuDecrypt = wx.Menu()

        self.decryptAES = wx.MenuItem(self.menuDecrypt, wx.ID_ANY, "AES")
        self.decryptDES = wx.MenuItem(self.menuDecrypt, wx.ID_ANY, "DES")
        self.decryptDES3 = wx.MenuItem(self.menuDecrypt, wx.ID_ANY, "DES Triple")
        self.decryptARC2 = wx.MenuItem(self.menuDecrypt, wx.ID_ANY, "ARC2")
        self.decryptARC4 = wx.MenuItem(self.menuDecrypt, wx.ID_ANY, "ARC4")
        self.decryptBlowfish = wx.MenuItem(self.menuDecrypt, wx.ID_ANY, "Blowfish")
        self.decryptCAST = wx.MenuItem(self.menuDecrypt, wx.ID_ANY, "CAST")

        self.menuDecrypt.Append(self.decryptAES)
        self.menuDecrypt.Append(self.decryptDES)
        self.menuDecrypt.Append(self.decryptDES3)
        self.menuDecrypt.Append(self.decryptARC2)
        self.menuDecrypt.Append(self.decryptARC4)
        self.menuDecrypt.Append(self.decryptBlowfish)
        self.menuDecrypt.Append(self.decryptCAST)

        self.menuBar.Append(self.menuHash, "Hash")
        self.menuBar.Append(self.menuEncrypt, "Encryption")
        self.menuBar.Append(self.menuDecrypt, "Decryption")

        self.SetMenuBar(self.menuBar)

        self.Bind(wx.EVT_MENU, self.OnHashMD2, id=self.hashMD2.GetId())
        self.Bind(wx.EVT_MENU, self.OnHashMD4, id=self.hashMD4.GetId())
        self.Bind(wx.EVT_MENU, self.OnHashMD5, id=self.hashMD5.GetId())
        self.Bind(wx.EVT_MENU, self.OnHashSHA1, id=self.hashSHA1.GetId())
        self.Bind(wx.EVT_MENU, self.OnHashSHA224, id=self.hashSHA224.GetId())
        self.Bind(wx.EVT_MENU, self.OnHashSHA256, id=self.hashSHA256.GetId())
        self.Bind(wx.EVT_MENU, self.OnHashSHA384, id=self.hashSHA384.GetId())
        self.Bind(wx.EVT_MENU, self.OnHashSHA512, id=self.hashSHA512.GetId())
        self.Bind(wx.EVT_MENU, self.OnHashRIPEMD, id=self.hashRIPEMD.GetId())

        #Symmetric Encrypt
        self.Bind(wx.EVT_MENU, self.OnEnAES, id=self.encryptAES.GetId())
        self.Bind(wx.EVT_MENU, self.OnEnDES, id=self.encryptDES.GetId())
        self.Bind(wx.EVT_MENU, self.OnEnARC2, id=self.encryptDES3.GetId())
        self.Bind(wx.EVT_MENU, self.OnEnARC4, id=self.encryptARC4.GetId())
        self.Bind(wx.EVT_MENU, self.OnEnBlowfish, id=self.encryptBlowfish.GetId())
        self.Bind(wx.EVT_MENU, self.OnEnCAST, id=self.encryptCAST.GetId())

        self.btnChooseFile.Bind(wx.EVT_BUTTON, self.chooseFile)

        self.Centre(wx.BOTH)

    def __del__(self):
        pass

    def chooseFile(self, event):
        dlg = wx.FileDialog(self, message='Choose a file', wildcard='*.*')
        if dlg.ShowModal() == wx.ID_OK:
            self.txtFile.SetLabel(dlg.GetFilename())
            self.filePath = dlg.GetPath()
            pass

    def showDialogKey(self):
        dlg = wx.TextEntryDialog(self, message="Please enter the key:", caption="nothing")
        if dlg.ShowModal() == wx.ID_OK:
            self.key = dlg.GetValue()
            print(self.key)

    def showDialogHash(self, hashvalue):
        dlg = wx.MessageDialog(self, message=hashvalue.upper(), caption="The hash value", style=wx.ICON_NONE)
        dlg.ShowModal()

    # Hash
    def OnHashMD2(self, event):
        self.txtWelcome.SetLabel("Hash MD2")
        if self.filePath is not "":
            hashvalue = Hash.hashfile(self.filePath, "MD2")

            self.showDialogHash(hashvalue)
    def OnHashMD4(self, event):
        self.txtWelcome.SetLabel("Hash MD4")
        if self.filePath is not "":
            hashvalue = Hash.hashfile(self.filePath, "MD4")

            self.showDialogHash(hashvalue)
    def OnHashMD5(self, event):
        self.txtWelcome.SetLabel("Hash MD5")
        if self.filePath is not "":
            hashvalue = Hash.hashfile(self.filePath, "MD5")

            self.showDialogHash(hashvalue)
    def OnHashSHA1(self, event):
        self.txtWelcome.SetLabel("Hash SHA1")
        if self.filePath is not "":
            hashvalue = Hash.hashfile(self.filePath, "SHA1")

            self.showDialogHash(hashvalue)
    def OnHashSHA224(self, event):
        self.txtWelcome.SetLabel("Hash SHA224")
        if self.filePath is not "":
            hashvalue = Hash.hashfile(self.filePath, "SHA224")

            self.showDialogHash(hashvalue)
    def OnHashSHA256(self, event):
        self.txtWelcome.SetLabel("Hash SHA256")
        if self.filePath is not "":
            hashvalue = Hash.hashfile(self.filePath, "SHA256")

            self.showDialogHash(hashvalue)
    def OnHashSHA384(self, event):
        self.txtWelcome.SetLabel("Hash SHA384")
        if self.filePath is not "":
            hashvalue = Hash.hashfile(self.filePath, "SHA384")

            self.showDialogHash(hashvalue)
    def OnHashSHA512(self, event):
        self.txtWelcome.SetLabel("Hash SHA512")
        if self.filePath is not "":
            hashvalue = Hash.hashfile(self.filePath, "SHA512")

            self.showDialogHash(hashvalue)
    def OnHashRIPEMD(self, event):
        self.txtWelcome.SetLabel("Hash RIPEMD160")
        if self.filePath is not "":
            hashvalue = Hash.hashfile(self.filePath, "RIPEMD")

            self.showDialogHash(hashvalue)

    #Symmetric Encrypt/Decrypt
    def OnEnAES(self, event):
        self.txtWelcome.SetLabel("Symmetric Encrypt AES")
        self.showDialogKey()

        if self.filePath is not "":
            # Hien thuc
            dlg = ttMessageDialog(message="Successful", title="title")
            dlg.ShowModal()
    def OnEnDES(self, event):
        self.txtWelcome.SetLabel("Symmetric Encrypt DES")
        self.showDialogKey()

        if self.filePath is not "":
            # Hien thuc
            dlg = ttMessageDialog(message="Successful", title="title")
            dlg.ShowModal()
    def OnEnDES3(self, event):
        self.txtWelcome.SetLabel("Symmetric Encrypt DES3")
        self.showDialogKey()

        if self.filePath is not "":
            # Hien thuc
            dlg = ttMessageDialog(message="Successful", title="title")
            dlg.ShowModal()
    def OnEnARC2(self, event):
        self.txtWelcome.SetLabel("Symmetric Encrypt ARC2")
        self.showDialogKey()

        if self.filePath is not "":
            # Hien thuc
            dlg = ttMessageDialog(message="Successful", title="title")
            dlg.ShowModal()
    def OnEnARC4(self, event):
        self.txtWelcome.SetLabel("Symmetric Encrypt ARC4")
        self.showDialogKey()

        if self.filePath is not "":
            # Hien thuc
            dlg = ttMessageDialog(message="Successful", title="title")
            dlg.ShowModal()
    def OnEnCAST(self, event):
        self.txtWelcome.SetLabel("Symmetric Encrypt CAST")
        self.showDialogKey()

        if self.filePath is not "":
            # Hien thuc
            dlg = ttMessageDialog(message="Successful", title="title")
            dlg.ShowModal()
    def OnEnBlowfish(self, event):
        self.txtWelcome.SetLabel("Symmetric Encrypt Blowfish")
        self.showDialogKey()

        if self.filePath is not "":
            # Hien thuc
            dlg = ttMessageDialog(message="Successful", title="title")
            dlg.ShowModal()


app = wx.App()
frame = MainFrame(None)
frame.Show()
app.MainLoop()
