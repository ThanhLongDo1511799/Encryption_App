from Crypto.PublicKey import RSA
from Crypto import Random


class RSACipher():
    readsize = 127
    writesize = 128
    random_generator = Random.new().read
    private_key = RSA.generate(writesize*8,random_generator)
    public_key = private_key.publickey()

    def encryptRSA(self,filename):

        infile = open(filename, 'rb')
        outfile = open('RSA'+filename+'.enc', 'wb')
        i = 0
        while True:
            data = infile.read(self.readsize)
            if not data:
                break
            i += 1
            enc_data = self.public_key.encrypt(data, 32)[0]
            while len(enc_data) <self.writesize:
                enc_data = "\x00" + enc_data
            outfile.write(enc_data)
        print(i)
        outfile.close()
        infile.close()

    def decryptRSA(self, filename):

        infile = open(filename, 'rb')
        outfile = open(filename+'.denc', 'wb')
        j = 0
        while True:
            data = infile.read(self.writesize)
            if not data:
                break
            j += 1
            dec_data =self.private_key.decrypt(data)
            outfile.write(dec_data[:self.readsize])
        print(j)
        outfile.close()
        infile.close()

a = RSACipher()
a.encryptRSA('to_enc.txt')
a.decryptRSA('ENRSAto_enc.txt')



 




