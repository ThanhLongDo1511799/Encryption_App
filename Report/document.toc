\contentsline {section}{\numberline {1}T\IeC {\'o}m t\IeC {\'\abreve }t}{2}{section.1}
\contentsline {section}{\numberline {2}Gi\IeC {\'\ohorn }i thi\IeC {\d \ecircumflex }u}{2}{section.2}
\contentsline {section}{\numberline {3}C\IeC {\'a}c gi\IeC {\h a}i thu\IeC {\d \acircumflex }t s\IeC {\h \uhorn } d\IeC {\d u}ng trong \IeC {\'\UHORN }ng d\IeC {\d u}ng}{2}{section.3}
\contentsline {subsection}{\numberline {3.1}Hash}{2}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}sysmetric algorithm}{3}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}asymetric algorithm}{4}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Public Key Cryptography}{4}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}M\IeC {\^o} t\IeC {\h a} gi\IeC {\h a}i thu\IeC {\d \acircumflex }t RSA}{5}{subsubsection.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.2}Hi\IeC {\d \ecircumflex }n th\IeC {\d \uhorn }c trong \IeC {\'\UHORN }ng d\IeC {\d u}ng}{5}{subsubsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.3}T\IeC {\'\i }nh an to\IeC {\`a}n c\IeC {\h u}a gi\IeC {\h a}i thu\IeC {\d \acircumflex }t}{5}{subsubsection.3.4.3}
\contentsline {subsection}{\numberline {3.5}Gi\IeC {\h a}i thu\IeC {\d \acircumflex }t DES}{6}{subsection.3.5}
\contentsline {subsubsection}{\numberline {3.5.1}M\IeC {\^o} t\IeC {\h a} gi\IeC {\h a}i thu\IeC {\d \acircumflex }t}{6}{subsubsection.3.5.1}
\contentsline {subsubsection}{\numberline {3.5.2}G\IeC {\h i}ai thu\IeC {\d \acircumflex }t triple DES3}{6}{subsubsection.3.5.2}
\contentsline {subsubsection}{\numberline {3.5.3}Hi\IeC {\d \ecircumflex }n th\IeC {\d \uhorn }c DES v\IeC {\`a} triple DES trong \IeC {\'\uhorn }ng d\IeC {\d u}ng}{7}{subsubsection.3.5.3}
\contentsline {subsubsection}{\numberline {3.5.4}\IeC {\DJ }\IeC {\d \ocircumflex } an to\IeC {\`a}n c\IeC {\h u}a gi\IeC {\h a}i thu\IeC {\d \acircumflex }t}{8}{subsubsection.3.5.4}
\contentsline {subsection}{\numberline {3.6}Gi\IeC {\h a}i thu\IeC {\d \acircumflex }t AES}{8}{subsection.3.6}
\contentsline {subsubsection}{\numberline {3.6.1}M\IeC {\^o} t\IeC {\h a} thu\IeC {\d \acircumflex }t to\IeC {\'a}n}{8}{subsubsection.3.6.1}
\contentsline {subsubsection}{\numberline {3.6.2}Hi\IeC {\d \ecircumflex }n th\IeC {\uhorn }c gi\IeC {\h a}i thu\IeC {\d \acircumflex }t}{10}{subsubsection.3.6.2}
\contentsline {subsubsection}{\numberline {3.6.3}T\IeC {\'\i }nh an to\IeC {\`a}n c\IeC {\h u}a AES}{10}{subsubsection.3.6.3}
\contentsline {section}{\numberline {4}C\IeC {\'a}c ch\IeC {\'\uhorn }c n\IeC {\u a}ng c\IeC {\h u}a ch\IeC {\uhorn }\IeC {\ohorn }ng tr\IeC {\`\i }nh}{10}{section.4}
\contentsline {section}{\numberline {5}Ph\IeC {\^a}n t\IeC {\'\i }ch v\IeC {\`a} k\IeC {\'\ecircumflex }t lu\IeC {\d \acircumflex }n}{11}{section.5}
\contentsline {section}{\numberline {6}H\IeC {\uhorn }\IeC {\'\ohorn }ng ph\IeC {\'a}t tri\IeC {\h \ecircumflex }n}{11}{section.6}
\contentsline {section}{\numberline {7}Tham Kh\IeC {\h a}o}{11}{section.7}
\contentsline {section}{\numberline {8}Ph\IeC {\d u} l\IeC {\d u}c}{11}{section.8}
\contentsline {subsection}{\numberline {8.1}Ph\IeC {\^a}n chia c\IeC {\^o}ng vi\IeC {\d \ecircumflex }c}{11}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}H\IeC {\uhorn }\IeC {\'\ohorn }ng d\IeC {\~\acircumflex }n s\IeC {\h \uhorn } d\IeC {\d u}ng ch\IeC {\uhorn }\IeC {\ohorn }ng tr\IeC {\`\i }nh}{11}{subsection.8.2}
\contentsline {subsubsection}{\numberline {8.2.1}Hash m\IeC {\d \ocircumflex }t file}{11}{subsubsection.8.2.1}
\contentsline {subsubsection}{\numberline {8.2.2}M\IeC {\~a} h\IeC {\'o}a file}{13}{subsubsection.8.2.2}
