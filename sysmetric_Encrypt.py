import os
from hash import *
from Crypto.Cipher import *

#each class will call pycrypto API sysmetric algorithm
#have 2 method: encrypt, decrypt
class AES_object:
	def __init__(self,password):
		key=hashstring(password,"MD5")
		self.encryptor=AES.new(key,AES.MODE_ECB)
	def encrypt(self,filename):
		chunksize=64*1024
		outfile="(AES)"+filename
		filesize=str(os.path.getsize(filename)).zfill(16)

		with open(filename,'rb') as infile:
			with open(outfile,'wb') as outfile:
				outfile.write(filesize)
				while True:
					chunk=infile.read(chunksize)
					if len(chunk)==0:
						break
					elif len(chunk)%16!=0:
						chunk+=' '*(16-(len(chunk)%16))
					outfile.write(self.encryptor.encrypt(chunk))
	def decrypt(self,filename):
		chunksize=64*1024
		plaintext=filename[5:]

		with open(filename,'rb') as cipher:
			with open(plaintext,'wb') as plaint:
				filesize=long(cipher.read(16))
				while True:
					chunk=cipher.read(chunksize)
					if len(chunk)==0:
						break
					plaint.write(self.encryptor.decrypt(chunk))
				plaint.truncate(filesize)


class DES_object:
	def __init__(self,password):
		key=hashstring(password,"MD2")
		self.encryptor=DES3.new(key,DES3.MODE_ECB)

	def encrypt(self,filename):
		chunksize=64*1024
		outfile="(DES)"+filename
		filesize=str(os.path.getsize(filename)).zfill(16)

		with open(filename,'rb') as infile:
			with open(outfile,'wb') as outfile:
				outfile.write(filesize)
				while True:
					chunk=infile.read(chunksize)
					if len(chunk)==0:
						break
					elif len(chunk)%16!=0:
						chunk+=' '*(16-(len(chunk)%16))
					outfile.write(self.encryptor.encrypt(chunk))

	def decrypt(self,filename):
		chunksize=64*1024
		plaintext=filename[6:]

		with open(filename,'rb') as cipher:
			with open(plaintext,'wb') as plaint:
				filesize=long(cipher.read(16))
				while True:
					chunk=cipher.read(chunksize)
					if len(chunk)==0:
						break
					plaint.write(self.encryptor.decrypt(chunk))
				plaint.truncate(filesize)

class DES3_object:
	def __init__(self,password):
		key=hashstring(password,"SHA1")[0:8]
		self.encryptor=DES.new(key,DES.MODE_ECB)

	def encrypt(self,filename):
		chunksize=64*1024
		outfile="(DES3)"+filename
		filesize=str(os.path.getsize(filename)).zfill(16)

		with open(filename,'rb') as infile:
			with open(outfile,'wb') as outfile:
				outfile.write(filesize)
				while True:
					chunk=infile.read(chunksize)
					if len(chunk)==0:
						break
					elif len(chunk)%16!=0:
						chunk+=' '*(16-(len(chunk)%16))
					outfile.write(self.encryptor.encrypt(chunk))

	def decrypt(self,filename):
		chunksize=64*1024
		plaintext=filename[6:]

		with open(filename,'rb') as cipher:
			with open(plaintext,'wb') as plaint:
				filesize=long(cipher.read(16))
				while True:
					chunk=cipher.read(chunksize)
					if len(chunk)==0:
						break
					plaint.write(self.encryptor.decrypt(chunk))
				plaint.truncate(filesize)

class ARC2_object:
	def __init__(self,password):
		key=hashstring(password,"SHA1")
		self.encryptor=ARC2.new(key,ARC2.MODE_ECB)
	def encrypt(self,filename):
		chunksize=64*1024
		outfile="(ARC2)"+filename
		filesize=str(os.path.getsize(filename)).zfill(16)

		with open(filename,'rb') as infile:
			with open(outfile,'wb') as outfile:
				outfile.write(filesize)
				while True:
					chunk=infile.read(chunksize)
					if len(chunk)==0:
						break
					elif len(chunk)%16!=0:
						chunk+=' '*(16-(len(chunk)%16))
					outfile.write(self.encryptor.encrypt(chunk))
	def decrypt(self,filename):
		chunksize=64*1024
		plaintext=filename[6:]

		with open(filename,'rb') as cipher:
			with open(plaintext,'wb') as plaint:
				filesize=long(cipher.read(16))
				while True:
					chunk=cipher.read(chunksize)
					if len(chunk)==0:
						break
					plaint.write(self.encryptor.decrypt(chunk))
				plaint.truncate(filesize)

class ARC4_object:
	def __init__(self,password):
		key=hashstring(password,"SHA512")
		self.encryptor=ARC2.new(key)
	def encrypt(self,filename):
		chunksize=64*1024
		outfile="(ARC2)"+filename
		filesize=str(os.path.getsize(filename)).zfill(16)

		with open(filename,'rb') as infile:
			with open(outfile,'wb') as outfile:
				outfile.write(filesize)
				while True:
					chunk=infile.read(chunksize)
					if len(chunk)==0:
						break
					elif len(chunk)%16!=0:
						chunk+=' '*(16-(len(chunk)%16))
					outfile.write(self.encryptor.encrypt(chunk))
	def decrypt(self,filename):
		chunksize=64*1024
		plaintext=filename[6:]

		with open(filename,'rb') as cipher:
			with open(plaintext,'wb') as plaint:
				filesize=long(cipher.read(16))
				while True:
					chunk=cipher.read(chunksize)
					if len(chunk)==0:
						break
					plaint.write(self.encryptor.decrypt(chunk))
				plaint.truncate(filesize)

class Blowfish_object:
	def __init__(self,password):
		key=hashstring(password,"SHA384")
		self.encryptor=Blowfish.new(key)
	def encrypt(self,filename):
		chunksize=64*1024
		outfile="(Blowfish)"+filename
		filesize=str(os.path.getsize(filename)).zfill(16)

		with open(filename,'rb') as infile:
			with open(outfile,'wb') as outfile:
				outfile.write(filesize)
				while True:
					chunk=infile.read(chunksize)
					if len(chunk)==0:
						break
					elif len(chunk)%16!=0:
						chunk+=' '*(16-(len(chunk)%16))
					outfile.write(self.encryptor.encrypt(chunk))
	def decrypt(self,filename):
		chunksize=64*1024
		plaintext=filename[10:]

		with open(filename,'rb') as cipher:
			with open(plaintext,'wb') as plaint:
				filesize=long(cipher.read(16))
				while True:
					chunk=cipher.read(chunksize)
					if len(chunk)==0:
						break
					plaint.write(self.encryptor.decrypt(chunk))
				plaint.truncate(filesize)

class CAST_object:
	def __init__(self,password):
		key=hashstring(password,"MD5")
		self.encryptor=CAST.new(key,CAST.MODE_ECB)
	def encrypt(self,filename):
		chunksize=64*1024
		outfile="(CAST)"+filename
		filesize=str(os.path.getsize(filename)).zfill(16)

		with open(filename,'rb') as infile:
			with open(outfile,'wb') as outfile:
				outfile.write(filesize)
				while True:
					chunk=infile.read(chunksize)
					if len(chunk)==0:
						break
					elif len(chunk)%16!=0:
						chunk+=' '*(16-(len(chunk)%16))
					outfile.write(self.encryptor.encrypt(chunk))
	def decrypt(self,filename):
		chunksize=64*1024
		plaintext=filename[6:]

		with open(filename,'rb') as cipher:
			with open(plaintext,'wb') as plaint:
				filesize=long(cipher.read(16))
				while True:
					chunk=cipher.read(chunksize)
					if len(chunk)==0:
						break
					plaint.write(self.encryptor.decrypt(chunk))
				plaint.truncate(filesize)

AES_object("hfyytrytr").encrypt("nothing.txt")
